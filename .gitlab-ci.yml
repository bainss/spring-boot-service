image: gradle:jdk11
include:
  - template: SAST.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml

code_quality:
  image: docker:stable
  variables:
    DOCKER_DRIVER: overlay2
    SP_VERSION: 0.85.6
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - docker run
        --env SOURCE_CODE="$PWD"
        --volume "$PWD":/code
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/ci-cd/codequality:$SP_VERSION" /code
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
      
before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle

stages:
  - assemble
  - fetch-version
  - test
  - build
  - dockerPush
  - release
  
fetch-semantic-version:
  # Requires Node >= 10.13 version
  image: node:13
  stage: fetch-version
  only:
    refs:
    - master
    - alpha
    - /^(([0-9]+)\.)?([0-9]+)\.x/ # This matches maintenance branches
    - /^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$/ # This matches pre-releases
  script:
    - npm install @semantic-release/gitlab @semantic-release/exec
    - npx semantic-release --generate-notes false --dry-run
  artifacts:
    paths:
    - VERSION.txt

generate-non-semantic-version:
  stage: fetch-version
  except:
    refs:
    - master
    - alpha
    - /^(([0-9]+)\.)?([0-9]+)\.x/ # This matches maintenance branches
    - /^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$/ # This matches pre-releases
  script:
    - echo build-$CI_PIPELINE_ID > VERSION.txt
  artifacts:
    paths:
    - VERSION.txt

assemble:
  stage: assemble
  script:
    - gradle assemble
  only:
    changes:
      - "*.gradle"
      - gradle.properties
  cache:
    key: gradle-cache-key
    paths:
      - .gradle/wrapper
      - .gradle/caches
    policy: push
  
cache:
  key: gradle-cache-key
  paths:
    - .gradle/wrapper
    - .gradle/caches
  policy: pull


build:
  stage: build
  script:
    - echo "Nexy Version will be $(cat VERSION.txt)"
    - gradle build
  artifacts:
    paths:
      - build/libs/*.jar

test:
  stage: test
  script:
    - gradle test
  artifacts:
    reports:
      junit: build/test-results/test/**/TEST-*.xml



package:
  stage: dockerPush
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  only:
    - tags    

release:
  image: node:13
  stage: release
  only:
    refs:
    - master
    - alpha
    # This matches maintenance branches
    - /^(([0-9]+)\.)?([0-9]+)\.x/
    # This matches pre-releases
    - /^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$/ 
  script:
    - touch CHANGELOG.md
    - npm install @semantic-release/gitlab @semantic-release/changelog
    - npx semantic-release
  artifacts:
    paths:
    - CHANGELOG.md    
    
